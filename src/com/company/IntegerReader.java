package com.company;
import java.util.List;

public interface IntegerReader {
    /**
     * Returns integers from console.
     * @return List with integers.
     */
    int[] getIntsFromConsole();
}
