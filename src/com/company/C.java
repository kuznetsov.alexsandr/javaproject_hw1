package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] firstArr = new int[n];
        for (int i = 0; i < n; i++) {
            firstArr[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int[] secondArr = new int[m];
        for (int i = 0; i < m; i++) {
            secondArr[i] = scanner.nextInt();
        }
        int k = scanner.nextInt();

        for (int i = 0; i < m; i++) {
            secondArr[i] = k - secondArr[i];
        }

        int i = 0;
        int j = m-1;
        k = 0;
        while (j>=0 && i<n)
        {
            if(secondArr[j] < firstArr[i])
                j--;
            else if (secondArr[j] > firstArr[i])
                i++;
            else
            {
                k++;
                i++;
                j--;
            }
        }
        System.out.print(k);
        scanner.close();
        //SECOND way
        /*
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(bufferedReader.readLine());
            String[] firstStr = bufferedReader.readLine().split(" ");
            int m = Integer.parseInt(bufferedReader.readLine());
            String[] secondStr = bufferedReader.readLine().split(" ");
            int k = Integer.parseInt(bufferedReader.readLine());

            int[] firstArr = new int[n];
            int[] secondArr = new int[m];
            int max = Math.max(n,m);

            for (int i = 0; i < n; i++) {
                firstArr[i] = Integer.parseInt(firstStr[i]);
            }
            for (int i = 0; i < m; i++) {
                secondArr[i] = k - Integer.parseInt(secondStr[i]);
            }

            int i = 0;
            k = 0;
            while ((i<n) && (i<m) && firstArr[i] == secondArr[m-i-1] )
            {
                k++;
                i++;
            }
            System.out.print(k);
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
