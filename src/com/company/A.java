package com.company;
import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(bufferedReader.readLine());
            String[] firstStr = bufferedReader.readLine().split(" ");
            String[] secondStr = bufferedReader.readLine().split(" ");
            int[] firstArr = new int[n];
            int[] secondArr = new int[n];
            for (int i = 0; i < n; i++) {
                firstArr[i] = Integer.parseInt(firstStr[i]);
                secondArr[i] = Integer.parseInt(secondStr[i]);
            }
//            System.out.print("1 = " + Arrays.toString(firstArr));
//            System.out.print("2 = " + Arrays.toString(secondArr));

            int maxI = 0;
            int k = 0;
            int i = 0;
            int j = 0;
            int i_ans = 0;
            int j_ans = 0;
            while (j < n)
            {
                while (j+1<n && secondArr[j+1]>secondArr[j])
                {
                    j++;
                }
                while (i<j)
                {
                    i++;
                    if (firstArr[i]> firstArr[maxI])
                        maxI = i;
                }
                if(secondArr[j_ans]+firstArr[i_ans] < secondArr[j]+firstArr[maxI])
                {
                    i_ans = maxI;
                    j_ans = j;
                }
                j++;
            }
            System.out.print(i_ans + " " + j_ans);


            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
