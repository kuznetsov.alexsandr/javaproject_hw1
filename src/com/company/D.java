package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class D {
    public static void main(String[] args) {
        try {
//            File fil = new File("input.txt");
//            FileReader inputFil = new FileReader(fil);
//            BufferedReader in = new BufferedReader(inputFil);
//            Scanner scanner = new Scanner(new File("/Users/alexandrkuznetsov/FirstHomework/src/input.txt"));
//            int n = scanner.nextInt();
//            int m = scanner.nextInt();
//            System.out.print(n + " " + m);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            int n,m;
            String[] inpNumbers = bufferedReader.readLine().split(" ");
            n = Integer.parseInt(inpNumbers[0]);
            m = Integer.parseInt(inpNumbers[1]);

            ArrayList<Integer> list = new ArrayList<Integer>(n);

            for (int j = 1; j <= n; j++) {
                list.add(j);
            }
            int i = m-1;
            while(list.size()!=1)
            {
                list.remove(i);
                i = (i+m-1) % list.size();

            }
            System.out.print(list.get(0));
            bufferedReader.close();
//            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
