package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class B {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [] x = new int[n];
        int [] y = new int[n];

        for (int i = 0; i < n; i++) {
            x[i] = scanner.nextInt();
            y[i] = scanner.nextInt();
        }

        double squares;
        double res = 0;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                squares = x[i] * (y[n - 1] - y[i + 1]);
            } else if (i == n - 1) {
                squares = x[i] * (y[i - 1] - y[0]);
            } else {
                squares = x[i] * (y[i - 1] - y[i + 1]);
            }
            res += squares;
        }
        System.out.print(res/2);
        scanner.close();
    }
}

